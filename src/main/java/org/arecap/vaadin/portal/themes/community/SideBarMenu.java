/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.themes.community;


import com.vaadin.event.MouseEvents;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.spring.endpoint.util.ComponentUtil;
import com.vaadin.spring.frontend.shared.data.WebResourceReferenceLocatorBiFunctionVisitor;
import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;
import com.vaadin.spring.frontend.ui.FrameContainer;
import com.vaadin.spring.frontend.ui.FrameContainerUtil;
import com.vaadin.ui.UI;
import org.arecap.vaadin.portal.themes.boot.NavigationProperties;
import org.arecap.vaadin.portal.themes.community.constants.ThemeColors;
import org.arecap.vaadin.portal.ui.SideWrapper;
import org.contextualj.lang.annotation.expression.SourceClass;
import org.contextualj.lang.annotation.expression.SourceName;
import org.contextualj.lang.annotation.pointcut.HandlingReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cop.annotation.ContextOriented;

import java.util.Optional;

@ContextOriented
@SourceClass(CommunityTemplate.class)
@ViewScope
public final class SideBarMenu implements WebResourceReferenceLocatorBiFunctionVisitor<NavigationProperties, SideBarMenu.OptionFrameBuilder> {

  static final class OptionFrameBuilder {




    static FrameContainer getTopOption(NavigationProperties option, String path, FrameContainer category, int index, int levelSize) {
      FrameContainer topOption =
              FrameContainerUtil.getStyleFrame("toplevelcategory",
                      ThemeColors.values()[index % 4].name().toLowerCase(),
                      (index == 0 ? "first" :  (index == levelSize - 1 ? "last" : "")));
      category.addComponent(topOption);
      FrameContainer categoryHeader =
              FrameContainerUtil.getStyleFrame(ThemeColors.values()[index % 4].name().toLowerCase(),
                      "toplevelcategory-header");
      topOption.addComponent(categoryHeader);
      FrameContainer categoryHeaderAnchor = getAnchorStyleFrame("toplevelcategory");
      categoryHeaderAnchor.setInnerHtml(getOptionIcon(option));
      categoryHeaderAnchor.setInnerText(option.getText());
      categoryHeader.addComponent(categoryHeaderAnchor);
      topOption.addClickListener((MouseEvents.ClickListener) event -> {
        CommunityTemplate.setLocationUriFragment(path);
      });
      return topOption;
    }

    static FrameContainer getLevelOption(NavigationProperties option, String path, FrameContainer categoryTop, boolean lowest) {
      FrameContainer levelOption = FrameContainerUtil.getStyleFrame(lowest ? "lowestlevelcategory" : "secondlevelcategory");
      categoryTop.addComponent(levelOption);
      FrameContainer levelOptionAnchor = getAnchorStyleFrame(lowest ? "lowestlevelcategory" : "secondlevelcategory");
      levelOptionAnchor.setInnerText(option.getText());
      levelOption.addComponent(levelOptionAnchor);
      levelOption.addClickListener((MouseEvents.ClickListener) event -> {
        CommunityTemplate.setLocationUriFragment(path);
      });
      return levelOption;
    }

    static FrameContainer getOptionLink(NavigationProperties option, FrameContainer levelOption) {
      FrameContainer optionLink = FrameContainerUtil.getStyleFrame("gwt-HTML", "samplelink");
      optionLink.setInnerHtml(getOptionIconTitle(option));
      optionLink.addClickListener((MouseEvents.ClickListener) event -> UI.getCurrent().getPage().setLocation(option.getHyperlink()));
      levelOption.addComponent(optionLink);
      return optionLink;
    }


    static FrameContainer getAnchorStyleFrame(String styleName) {
      return FrameContainerUtil.getStyleFrame("gwt-HTML", styleName+"-anchor");
    }

    static String getOptionIcon(NavigationProperties option) {
      return "<span class=\"icon\">&#x"+Integer.toHexString(option.getCodepoint())+";</span>";
    }

    static String getOptionTitle(NavigationProperties option) {
      return "<span class=\"title\">"+option.getText()+"</span>";
    }

    static String getOptionIconTitle(NavigationProperties option) {
      return getOptionIcon(option) + getOptionTitle(option);
    }


    private final NavigationProperties option;
    private final String relPath;
    private final FrameContainer parent;
    private final int level;
    private int index;
    private final int levelSize;
    private final FrameContainer frameContainer;



     OptionFrameBuilder(NavigationProperties option, String relPath, FrameContainer parent, int level, int index, int levelSize) {
      this.option = option;
      this.relPath = relPath;
      this.parent = parent;
      this.level = level;
      this.index = index;
      this.levelSize = levelSize;
      this.frameContainer = build();
    }

    public FrameContainer getFrameContainer() {
      return frameContainer;
    }

    public int getLevel() {
      return level;
    }

    public int getIndex() {
      return index++;
    }

    public int getLevelSize() {
      return levelSize;
    }

    public String getRelPath() {
      return relPath;
    }

    protected FrameContainer build() {
      if(level == 0) {
        return parent;
      }
      if(level == 1) {
        return getTopOption(option, relPath, parent, index, levelSize);
      }
      if(option.getWebResourceReferences() == null || option.getWebResourceReferences().isEmpty()) {
        return getOptionLink(option, parent);
      }
      return getLevelOption(option, relPath, parent,option.getWebResourceReferences().values().stream()
              .filter(entry -> entry.getWebResourceReferences() != null && !entry.getWebResourceReferences().isEmpty()).count() == 0);
    }

  }

  @Autowired
  private NavigationProperties navigationProperties;

  @HandlingReturn
  @SourceName("getSideWrapper")
  public void configure(SideWrapper sideWrapper) {
    sideWrapper.addSection("menu", "Menu", "sidebarbutton-menu", true);
    FrameContainer menu = (FrameContainer) ComponentUtil.findComponentById(sideWrapper,"menu");
    FrameContainer listing = FrameContainerUtil.getStyleFrame(StyleConstantUtil.TRANSITIONED);
    listing.removeFrameworkStyle();
    listing.setId("listing");
    menu.addComponent(listing);
    configure(listing);
  }

  protected void configure(FrameContainer listing) {
    FrameContainer categoryScrollPanel = FrameContainerUtil.getStyleFrame("categoryscrollpanel");
    categoryScrollPanel.setAttribute("style", "overflow: auto; position: relative; zoom: 1;");

    FrameContainer scrollContent = FrameContainerUtil.getStyleFrame();
    scrollContent.setAttribute("style", "position: relative; zoom: 1;");
    categoryScrollPanel.addComponent(scrollContent);

    FrameContainer categoryPanel = FrameContainerUtil.getStyleFrame("categorypanel");
    scrollContent.addComponent(categoryPanel);

    visit(navigationProperties, new OptionFrameBuilder(navigationProperties, "", categoryPanel, 0, 0,
                    Optional.ofNullable(navigationProperties.getWebResourceReferences()).map( m -> m.size()).orElse(0)),
            (frameBuilder, option) -> visitReference(option, frameBuilder));


    listing.addComponent(categoryScrollPanel);
  }

  public OptionFrameBuilder visitReference(NavigationProperties navigationProperties, OptionFrameBuilder optionFrameBuilder) {
    return new OptionFrameBuilder(navigationProperties, optionFrameBuilder.getRelPath()+"/"+navigationProperties.getRelationship(), optionFrameBuilder.getFrameContainer(),
            optionFrameBuilder.getLevel() + 1, optionFrameBuilder.getIndex(),
            Optional.ofNullable(navigationProperties.getWebResourceReferences()).map(m -> m.size()).orElse(0));
  }

}
