/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.themes;

import com.vaadin.spring.endpoint.annotation.GuiComponent;
import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;
import com.vaadin.spring.frontend.ui.FrameContainer;
import com.vaadin.spring.template.annotation.TemplateDelegate;
import org.springframework.core.annotation.Order;

@TemplateDelegate
@Order(0)
public class BrowserTemplate {

    @GuiComponent(id="browser", renderOrder = 0)
    public FrameContainer getBrowser() {
        FrameContainer frameContainer = new FrameContainer();
        frameContainer.addStyleName("still");
        frameContainer.setSizeFull();
        return frameContainer;
    }

    @GuiComponent(id="browsercontent", section = "browser", renderOrder = 1)
    public FrameContainer getBrowserContent() {
        FrameContainer frameContainer = new FrameContainer();
        frameContainer.setStyleName(StyleConstantUtil.TRANSITIONED);
        frameContainer.removeFrameworkStyle();
        return frameContainer;
    }

}
