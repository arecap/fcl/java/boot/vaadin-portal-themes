/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.themes.boot;

import com.vaadin.spring.frontend.shared.data.WebResourceReferenceLocator;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "navigation")
public class NavigationProperties implements WebResourceReferenceLocator<NavigationProperties> {

    private String rel;

    private String href;

    private String text;

    private String altText;

    private String fontFamily;

    private int codepoint;

    private Map<String, NavigationProperties> options;


    @Override
    public String getRelationship() {
        return rel;
    }

    @Override
    public String getHyperlink() {
        return href;
    }

    @Override
    public Map<String, NavigationProperties> getWebResourceReferences() {
        return options;
    }

    @Override
    public String getId() {
        return rel;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getAltText() {
        return altText;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getPlaceholder() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getSearch() {
        return null;
    }

    @Override
    public String getMimeType() {
        return null;
    }

    @Override
    public String getFontFamily() {
        return fontFamily;
    }

    @Override
    public int getCodepoint() {
        return codepoint;
    }

    @Override
    public String getHtml() {
        return null;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setAltText(String altText) {
        this.altText = altText;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public void setCodepoint(int codepoint) {
        this.codepoint = codepoint;
    }

    public Map<String, NavigationProperties> getOptions() {
        return options;
    }

    public void setOptions(Map<String, NavigationProperties> options) {
        this.options = options;
    }

}
