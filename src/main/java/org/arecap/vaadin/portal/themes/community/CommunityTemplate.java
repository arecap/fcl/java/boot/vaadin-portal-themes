/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.themes.community;

import com.vaadin.shared.EventId;
import com.vaadin.spring.endpoint.annotation.EventMapping;
import com.vaadin.spring.endpoint.annotation.GuiComponent;
import com.vaadin.spring.endpoint.ui.ApplicationControllerService;
import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;
import com.vaadin.spring.frontend.ui.FrameContainer;
import com.vaadin.spring.frontend.ui.FrameContainerUtil;
import com.vaadin.spring.template.annotation.TemplateDelegate;
import com.vaadin.ui.UI;
import org.arecap.vaadin.portal.themes.community.annotation.CommunityTheme;
import org.arecap.vaadin.portal.ui.SideWrapper;
import org.arecap.vaadin.portal.ui.SideWrapperController;
import org.contextualj.lang.annotation.expression.SourceAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestParam;

@TemplateDelegate
@SourceAnnotation(CommunityTheme.class)
@Order(1)
public class CommunityTemplate {

    private SideWrapper sideWrapper = new SideWrapper();

    @Autowired
    private ApplicationControllerService controllerService;

    @InitBinder
    public void onPageInit() {
        controllerService.setId("communitymainlayout");
        UI.getCurrent().addStyleName(StyleConstantUtil.TRANSITIONED);
    }

    @InitBinder
    public void setPath(@RequestParam String path) {
    }

    @GuiComponent(section = "browsercontent", id = "navigator", renderOrder = 4)
    public FrameContainer getNavigationMenu() {
        final FrameContainer navigationMenu =
                FrameContainerUtil.getStyleFrame(StyleConstantUtil.TRANSITIONED, StyleConstantUtil.NON_SELECTABLE);
        navigationMenu.setAttribute("style", "font-size: "+Math.min(UI.getCurrent().getPage().getBrowserWindowWidth(),
                UI.getCurrent().getPage().getBrowserWindowHeight()) + "px");
        return navigationMenu;
    }

    @GuiComponent(id="sidebarwrapper", section = "browser", renderOrder = 3)
    public SideWrapper getSideWrapper() {
        return this.sideWrapper;
    }

    @GuiComponent(section = "browser", renderOrder = 4)
    public SideWrapperController getSideWrapperController() {
        return new SideWrapperController(sideWrapper);
    }


    @EventMapping(section = "navigator", event = FrameContainer.ScreenChangeEvent.class, eventId = EventId.CHANGE)
    public void onScreenChange(FrameContainer navigationMenu) {
        navigationMenu.setAttribute("style", "font-size: "+Math.min(UI.getCurrent().getPage().getBrowserWindowWidth(),
                UI.getCurrent().getPage().getBrowserWindowHeight()) + "px");
    }

    public static void setLocationUriFragment(String path) {
        UI.getCurrent().getPage().setLocation(UI.getCurrent().getPage().getLocation().getPath()+"?path="+path);
    }


}
