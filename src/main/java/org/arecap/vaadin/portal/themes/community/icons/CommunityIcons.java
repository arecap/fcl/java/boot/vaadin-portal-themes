/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.themes.community.icons;

import com.vaadin.server.FontIcon;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public enum CommunityIcons implements FontIcon {

    DOCUMENT(0XE000),
    SEARCH(0XE001),
    SETTINGS(0XE002),
    VAADIN(0XE003),
    COMPONENT(0XE004),
    ARROW_LEFT(0XE005),
    ARROW_RIGHT(0XE006),
    ARROW_TOP(0XE007),
    SCROLL_DOWN(0XE008),
    SCROLL_UP(0XE009),
    RESIZE(0XE00A),
    SEVEN(0XE00B),
    OLAP(0XE00C),
    ENTER_CLICK(0XE00D),
    FOUNDATION(0XE00E),
    REMOTE_STORAGE(0XE00F),
    DRAG(0XE010),
    EDIT(0XE011),
    DATA_CHILD(0XE012),
    VERTICAL_LAYOUT(0XE012),
    NUMERIC(0XE014),
    CALENDAR(0XE015),
    CHART(0XE016),
    CALENDAR_(0XE017),
    CLUSTER_DATA_PROVIDER(0XE018),
    DATA_PROVIDER(0XE019),
    RESTART(0XE01A),
    USER_INTERACTION(0XE01B),
    MENU(0XE01C),
    SETTINGS_(0XE01D),
    CONTROL_INPUTS(0XE01E),
    SCROLLABLE(0XE01F),
    DOCUMENT_(0XE020),



    WEB_CONSOLE(0xE038),
    HUMAN(0xE602),
    ENGINE_PERFORMANCE(0xE608);

    private static final String fontFamily = "community";
    private int codepoint;

    CommunityIcons(int codepoint) {
        this.codepoint = codepoint;
    }

    @Override
    public String getMIMEType() {
        throw new UnsupportedOperationException(FontIcon.class.getSimpleName()
                + " should not be used where a MIME type is needed.");
    }

    @Override
    public String getFontFamily() {
        return fontFamily;
    }

    @Override
    public int getCodepoint() {
        return codepoint;
    }

    @Override
    public String getHtml() {
        return "<span class=\"v-icon v-icon-" + name().toLowerCase()
                + "\" style=\"font-family: " + fontFamily + ";\">&#x"
                + Integer.toHexString(codepoint) + ";</span>";
    }

}
