/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.themes.community.annotation;

import com.vaadin.spring.endpoint.annotation.GuiController;
import com.vaadin.spring.template.annotation.DelegateController;
import com.vaadin.ui.UI;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Marker annotation
 *
 * Indicates that an annotated target is a <i>component</i> of a
 * Template Component.
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@GuiController
@DelegateController
@CommunityTheme
@Documented
public @interface CommunityThemeController {


    @AliasFor(annotation = GuiController.class, attribute = "uri")
    String value() default "";

    @AliasFor(annotation = GuiController.class, attribute = "value")
    String uri() default "";

    @AliasFor(annotation = GuiController.class, attribute = "ui")
    Class<? extends UI> ui() default UI.class;

}
