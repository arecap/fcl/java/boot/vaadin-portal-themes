/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.themes.community;


import com.vaadin.event.MouseEvents;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.spring.frontend.shared.data.WebResourceReferenceLocatorBiFunctionVisitor;
import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;
import com.vaadin.spring.frontend.ui.FrameContainer;
import com.vaadin.spring.frontend.ui.FrameContainerUtil;
import com.vaadin.ui.UI;
import org.arecap.vaadin.portal.themes.boot.NavigationProperties;
import org.contextualj.lang.annotation.expression.SourceClass;
import org.contextualj.lang.annotation.expression.SourceName;
import org.contextualj.lang.annotation.pointcut.Bid;
import org.contextualj.lang.annotation.pointcut.HandlingReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cop.annotation.ContextOriented;

@ContextOriented
@SourceClass(CommunityTemplate.class)
@ViewScope
public final class NavigationMenu implements WebResourceReferenceLocatorBiFunctionVisitor<NavigationProperties, NavigationMenu.NavigationFrameBuilder> {


  @Override
  public NavigationFrameBuilder visitReference(NavigationProperties resourceReference, NavigationFrameBuilder input) {
    return new NavigationFrameBuilder(resourceReference, input.getRelPath()+"/"+resourceReference.getRelationship(),
            input.getLocationPath(), input.getParentCandidate(), input.getPrevious());
  }

  static final class NavigationFrameBuilder {

//    static FrameContainer getNode()


    static double zeroIfLess(double val) {
      return Math.abs(val) < 1.0E-3 ? 0.0 : val;
    }

    static String formatNf(double val, int n) {
      String strVal = val + "";
      return strVal.indexOf(".") == -1 ? strVal :
              strVal.substring(0, strVal.indexOf(".") + (strVal.indexOf(".") + n >= strVal.length() ? strVal.length() - strVal.indexOf(".") : n + 1));
    }

    private final NavigationProperties navigationProperties;

    private final String relPath;

    private final String locationPath;

    private final FrameContainer parent;

    private final FrameContainer frameContainer;

    private FrameContainer previous;

    private FrameContainer parentCandidate;

    NavigationFrameBuilder(NavigationProperties navigationProperties, String relPath, String locationPath, FrameContainer parent, FrameContainer previous) {
      this.navigationProperties = navigationProperties;
      this.relPath = relPath;
      this.locationPath = locationPath;
      this.parent = parent;
      this.previous = previous;
      this.frameContainer = renderForLocation(relPath) ? build() : null;
    }

    public String getRelPath() {
      return relPath;
    }

    public String getLocationPath() {
      return locationPath;
    }

    public FrameContainer getFrameContainer() {
      return frameContainer;
    }

    public FrameContainer getParentCandidate() {
      return parentCandidate;
    }

    public FrameContainer getPrevious() {
      return previous;
    }

    private boolean renderForLocation(String path) {
      if(path.length() > locationPath.length())
        return false;
      String[] relPaths = path.split("/");
      String[] locationPaths = locationPath.split("/");
      if(relPaths.length > locationPaths.length)
        return false;
      for(int i = 0; i < relPaths.length; i++) {
        if(!relPaths[i].equalsIgnoreCase(locationPaths[i])) {
          return false;
        }
      }
      return true;
    }


    protected FrameContainer build() {
      FrameContainer children = parent;
      FrameContainer node = null;
      if(relPath.isEmpty()) {
        node = FrameContainerUtil.getStyleFrame(StyleConstantUtil.NODE, StyleConstantUtil.TRANSITIONED, StyleConstantUtil.SELECTED, StyleConstantUtil.HIGHLIGHTED);
        parent.addComponent(node);
        FrameContainer wrapperPanel = FrameContainerUtil.getStyleFrame(StyleConstantUtil.WRAPPER_PANEL, StyleConstantUtil.TRANSITIONED);
        node.addComponent(wrapperPanel);
        children = FrameContainerUtil.getStyleFrame(StyleConstantUtil.CHILDREN, StyleConstantUtil.TRANSITIONED);
        wrapperPanel.addComponent(children);
        FrameContainer content = FrameContainerUtil.getStyleFrame(StyleConstantUtil.CONTENT, StyleConstantUtil.TRANSITIONED, StyleConstantUtil.HAS_ICON);
        this.previous = FrameContainerUtil.getStyleFrame(StyleConstantUtil.GWT_HTML, StyleConstantUtil.ICON_LABEL, StyleConstantUtil.TRANSITIONED);
        previous.setInnerHtml("&#x"+Integer.toHexString(navigationProperties.getCodepoint())+";");
        content.addComponent(previous);
        content.addComponent(FrameContainerUtil.getStyleFrame(StyleConstantUtil.GWT_HTML, StyleConstantUtil.TITLE_LABEL, StyleConstantUtil.TRANSITIONED));
        wrapperPanel.addComponent(content);
      } else {
        node = (FrameContainer) children.getParent().getParent();
        node.removeStyleName(StyleConstantUtil.CHILD);
        node.addStyleName(StyleConstantUtil.SELECTED);
        node.addStyleName(StyleConstantUtil.HIGHLIGHTED);
      }
      double angle = 0;
          int i = 0;
      for(NavigationProperties option: navigationProperties.getWebResourceReferences().values()) {
              i++;
        double top = zeroIfLess(Math.cos(angle)) * (-50);
        double left = zeroIfLess(Math.sin(angle)) * 50;
        FrameContainer childNode = FrameContainerUtil.getStyleFrame(StyleConstantUtil.NODE, StyleConstantUtil.TRANSITIONED, StyleConstantUtil.CHILD);
        childNode.setAttribute("style", "left: " + formatNf(left, 2)
                +"%; top: " + formatNf(top, 2)
                + "%; transform: rotate(0rad) translateX("
                + formatNf(left / 100, 2)
                + "em) translateY(" + formatNf(top / 100, 2)
                + "em);");
        children.addComponent(childNode);
        FrameContainer childWrapperPanel = FrameContainerUtil.getStyleFrame(StyleConstantUtil.WRAPPER_PANEL, StyleConstantUtil.TRANSITIONED);
        childNode.addComponent(childWrapperPanel);
        FrameContainer childChildren = FrameContainerUtil.getStyleFrame(StyleConstantUtil.CHILDREN, StyleConstantUtil.TRANSITIONED);
        childWrapperPanel.addComponent(childChildren);
        FrameContainer childContent = FrameContainerUtil.getStyleFrame(StyleConstantUtil.CONTENT, StyleConstantUtil.TRANSITIONED, StyleConstantUtil.HAS_ICON);
        childWrapperPanel.addComponent(childContent);
        FrameContainer childIcon = FrameContainerUtil.getStyleFrame(StyleConstantUtil.GWT_HTML, StyleConstantUtil.ICON_LABEL, StyleConstantUtil.TRANSITIONED);
        childIcon.setInnerHtml("&#x"+Integer.toHexString(option.getCodepoint())+";");
        childContent.addComponent(childIcon);
        FrameContainer childTitle = FrameContainerUtil.getStyleFrame(StyleConstantUtil.GWT_HTML, StyleConstantUtil.TITLE_LABEL, StyleConstantUtil.TRANSITIONED);
        childTitle.setInnerText(option.getText());
        childContent.addComponent(childTitle);
        if(renderForLocation(relPath+"/"+option.getRelationship())) {
          parentCandidate = childChildren;
            children.setAttribute("style", "transform: rotate(" + (Math.PI - angle) + "rad) translateX(0em) translateY(0em);");
            childWrapperPanel.setAttribute("style", "transform: rotate(" + (angle - Math.PI) + "rad) translateX(0em) translateY(0em);");
        }
        angle += (2 * Math.PI) / navigationProperties.getWebResourceReferences().size();
        if(option.getWebResourceReferences() != null && !option.getWebResourceReferences().isEmpty()) {
          childNode.addClickListener((MouseEvents.ClickListener) event ->
                  CommunityTemplate.setLocationUriFragment(relPath + "/" + option.getRelationship()));
        } else {
          childNode.addClickListener((MouseEvents.ClickListener) event -> UI.getCurrent().getPage().setLocation(option.getHyperlink()));
        }
      }
      if(parentCandidate != null) {
        node.removeStyleName(StyleConstantUtil.SELECTED);
        node.addStyleName("previous");
        if(locationPath.split("/").length  > relPath.split("/").length  + 1) {
          node.addStyleName("history");
        } else {
          previous.addClickListener((MouseEvents.ClickListener) event ->
                          CommunityTemplate.setLocationUriFragment(relPath));

        }
      }
      return node;
    }



  }


  @Autowired
  private NavigationProperties navigationProperties;

  private String path = "";

  @Bid
  @SourceName("setPath")
  public void setPath(String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }

  @HandlingReturn
  @SourceName("getNavigationMenu")
  public void configure(FrameContainer navigationMenuFrame) {
    visit(navigationProperties, new NavigationFrameBuilder(navigationProperties, "", path, navigationMenuFrame, null),
            (frameBuilder, option) -> visitReference(option, frameBuilder));
  }


  private double zeroIfLess(double val) {
    return Math.abs(val) < 1.0E-3 ? 0.0 : val;
  }

  private String formatNf(double val, int n) {
    String strVal = val + "";
    return strVal.indexOf(".") == -1 ? strVal :
            strVal.substring(0, strVal.indexOf(".") + (strVal.indexOf(".") + n >= strVal.length() ? strVal.length() - strVal.indexOf(".") : n + 1));
  }



}
