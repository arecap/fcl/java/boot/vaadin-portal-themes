/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.vaadin.portal.themes.support;

import org.arecap.vaadin.portal.themes.boot.NavigationProperties;

import java.util.Optional;
import java.util.function.Function;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
@Deprecated
public interface NavigationOptionFunctionVisitor  {

    default void visit(NavigationProperties navigationProperties, Function<NavigationProperties, Void> optionVisitorFunction) {
        Optional.of(navigationProperties).map(NavigationProperties::getRelationship).ifPresent(s -> {
            if (!s.equalsIgnoreCase("root")) {
                optionVisitorFunction.apply(navigationProperties);
            }
            visitOptions(navigationProperties, optionVisitorFunction);
        });
    }

    default void visitOptions(NavigationProperties navigationProperties, Function<NavigationProperties, Void> optionVisitorFunction) {
        Optional.ofNullable(navigationProperties.getWebResourceReferences()).ifPresent( topOptions -> {
            topOptions.values().stream().forEach(topOption -> {
                optionVisitorFunction.apply(topOption);
                Optional.ofNullable(topOption.getWebResourceReferences()).ifPresent( options -> {
                    options.values().stream().forEach( option -> visit(option, optionVisitorFunction));
                });
            });
        });
    }

    Void visitOptions(NavigationProperties navigationProperties);

}
